const authRouter = require("express").Router();
const path = require("path");
const querystring = require("querystring");

const passport = require("../lib/passport").passport;
const config = require("../config");

authRouter.get("/login", passport.authenticate("oidc"));

authRouter.get(
  "/callback",
  passport.authenticate("oidc", {
    successRedirect: "/restricted",
    failureRedirect: "/"
  })
);

authRouter.get("/logout", (req, res) => {
  // Log out of passport session.
  // Key remains in store, but passport object will become empty.
  req.logout();
  // Remove session from store entirely.
  req.session.destroy();

  const logoutUrl = new URL(
    `https://${path.join(config.oidcDomain, config.oidcLogoutPath)}`
  );
  logoutUrl.search = querystring.stringify({
    post_logout_redirect_uri: "http://localhost:4321"
  });
  return res.clearCookie(config.sessionCookieName).redirect(logoutUrl);
});

module.exports = authRouter;

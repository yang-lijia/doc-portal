const router = require("express").Router();
const assert = require("assert");
const path = require("path");
const { s3 } = require("../lib/aws");
const GitSource = require("../models/GitSource");
const DocsTable = require("../models/DocsTable");
router.post("/", async (req, res) => {
  // GitLab ignores status code returned.
  // Need to surface errors through other means
  let pushDetails = req.body;
  try {
    assert.strictEqual(pushDetails.object_kind, "push");
    assert.strictEqual(pushDetails.ref, "refs/heads/master");
  } catch (err) {
    return res.status(400).send("Webhook contains invalid payload!");
  }
  let projectId = pushDetails.project_id;
  let docFolderName = path.join(
    projectId.toString(),
    pushDetails.project.path_with_namespace.slice(
      pushDetails.project.path_with_namespace.lastIndexOf("/") + 1
    )
  ); // 123/my-project-path

  let docsDetails = await DocsTable.get({
    projectId: Number.parseInt(projectId)
  });
  let userEmail = docsDetails.userEmail;
  let projectFiles = await GitSource.getProjectFiles(
    userEmail,
    pushDetails.project_id
  );
  // Filter out tree (directory) objects
  projectFiles = projectFiles.filter(
    projectFileData => projectFileData.type === "blob"
  );
  // ["index.html", "README.md", "_sidebar.md"]
  let filePaths = projectFiles.map(file => file.path);
  let projectType = docsDetails.kind; // "public | restricted"

  // Possibly long operation. Fire and forget.
  filePaths.forEach(async filePath => {
    let fileDetails;
    try {
      fileDetails = await GitSource.getProjectFile(
        userEmail,
        projectId,
        filePath
      );
    } catch (err) {
      console.error(err.message);
      return res.status(500).send({});
    }
    let fileContent = fileDetails.content;
    let fileBuffer = Buffer.from(fileContent, "base64");
    let s3UploadParams = {
      Bucket: "devportal-docs",
      Key: path.join(projectType, docFolderName, filePath), // restricted/123/my-repo-name/README.md
      Body: fileBuffer
    };
    return new Promise((resolve, reject) => {
      s3.upload(s3UploadParams, (err, data) => {
        if (err) {
          console.error(err.message);
          return;
        }
        resolve(data);
      });
    });
  });

  // Fetch git files
  // Upload to S3 folder
  res.send("OK");
});

module.exports = router;

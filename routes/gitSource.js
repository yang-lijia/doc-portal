const router = require("express").Router();
const oauth2 = require("simple-oauth2");

const AppTable = require("../models/AppTable");
const DocsTable = require("../models/DocsTable");
const GitSource = require("../models/GitSource");
const config = require("../config");

const credentials = {
  client: {
    id: config.gitSourceAppId,
    secret: config.gitSourceAppSecret
  },
  auth: {
    tokenHost: "https://gitlab.com",
    tokenPath: "/oauth/token",
    // authorizeHost: "gitlab.com", // defaults to tokenHost
    authorizePath: "/oauth/authorize"
  }
};

const oauthClient = oauth2.create(credentials);

router.get("/authorize", (req, res) => {
  // Generate hash for state variable using user email and server-side key
  let authorizationUri = oauthClient.authorizationCode.authorizeURL({
    redirect_uri: "http://localhost:4321/git-source/callback",
    scope: "api read_repository openid profile email",
    state: "unique-value-for-each-request"
  });
  res.redirect(authorizationUri);
});

router.get("/callback", async (req, res) => {
  // Validate state variable tied to user email
  // Retrieve access token linked to user email, store in dynamodb.
  let authCode = req.query.code;
  let tokenConfig = {
    code: authCode,
    redirect_uri: "http://localhost:4321/git-source/callback",
    scope: "api read_repository openid profile email"
  };
  try {
    let tokens = await oauthClient.authorizationCode.getToken(tokenConfig);
    await AppTable.put({
      email: req.user.appUser.email,
      tokens
    });
    res.redirect("/account");
  } catch (error) {
    console.error(error);
  }
});

router.get("/projects", async (req, res) => {
  let userEmail = req.user.appUser.email;
  let userGitSourceProjects = await GitSource.getProjects(userEmail);
  res.send(userGitSourceProjects);
});

router.get("/projects/:projectId", async (req, res) => {
  let userEmail = req.user.appUser.email;
  let gitSourceDetails = await GitSource.getProject(
    userEmail,
    req.params.projectId
  );
  let projectDetails = await DocsTable.get({
    projectId: Number.parseInt(req.params.projectId)
  });
  res.send({
    ...gitSourceDetails,
    ...projectDetails
  });
});

router.get("/projects/:projectId/hooks", async (req, res) => {
  let projectHooks = await GitSource.getProjectHooks(
    req.user.appUser.email,
    req.params.projectId
  );
  let docPortalHooks = projectHooks.filter(
    projectHook => projectHook.url === config.gitSourceWebhookUrl
  );
  let projectDetails = await DocsTable.get({
    projectId: Number.parseInt(req.params.projectId)
  });
  res.send({
    hooks: docPortalHooks,
    ...projectDetails
  });
});

router.post("/projects/:projectId/hooks", async (req, res) => {
  // Set webhook for project, store type in DocsTable
  let addResult = await GitSource.addProjectHook(
    req.user.appUser.email,
    req.params.projectId
  );
  await DocsTable.put({
    projectId: Number.parseInt(req.params.projectId),
    kind: req.body.kind, // Use kind because type is often reserved, e.g. for dynamoClient
    userEmail: req.user.appUser.email
  });
  res.send({ hook: addResult, kind: req.body.kind });
});

router.delete("/projects/:projectId/hooks/:hookId", async (req, res) => {
  let deleteResult = await GitSource.deleteProjectHook(
    req.user.appUser.email,
    req.params.projectId,
    req.params.hookId
  );
  await DocsTable.delete({
    projectId: Number.parseInt(req.params.projectId)
  });

  // let projectDetails = await GitSource.getProject(
  //   req.user.appUser.email,
  //   req.params.projectId
  // );
  // let s3PathName = projectDetails.path;
  // let bucketParams = {

  // };
  // await new Promise((resolve, reject) => {
  //   s3.listObjectsV2(bucketParams, (err, data) => {
  //     if (err) {
  //       return reject(err);
  //     }
  //     console.log(data);
  //   });
  // });
  res.send(deleteResult);
});

module.exports = router;

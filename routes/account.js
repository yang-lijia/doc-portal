const _ = require("lodash");
const router = require("express").Router();
const AppTable = require("../models/AppTable");

router.get("/", async (req, res) => {
  let userEmail = req.user.appUser.email;
  let userAttributes = await AppTable.get({
    email: userEmail
  });
  let hasAuthorizedGitSource = true;
  if (!userAttributes.tokens || _.isEmpty(userAttributes.tokens)) {
    hasAuthorizedGitSource = false;
  }
  let renderOptions = {
    title: "Account",
    profile: req.user.profile,
    isAuthenticated: req.isAuthenticated(),
    hasAuthorizedGitSource
  };
  res.render("account", renderOptions);
});

module.exports = router;

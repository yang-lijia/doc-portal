var express = require("express");
var router = express.Router();
const path = require("path");
const DocsTable = require("../models/DocsTable");
const GitSource = require("../models/GitSource");
const secured = require("../lib/passport").secured;
const { s3 } = require("../lib/aws");

/* GET home page. */
router.get("/", async function(req, res) {
  let isAuthenticated = req.isAuthenticated();

  let publicProjects = await DocsTable.scan({
    kind: "public"
  });

  let projectDetails = await Promise.all(
    publicProjects.map(async publicProject => {
      let projectId = publicProject.projectId;
      let userEmail = publicProject.userEmail;
      return GitSource.getProject(userEmail, projectId);
    })
  );

  projectDetails.forEach(project => {
    project.docPortal = {
      path: path.join(`/public`, `${project.id}`, `${project.path}`)
    };
  });

  res.render("home", {
    title: "Home",
    projects: projectDetails,
    isAuthenticated
  });
});

router.get(`/public/:projectId/:projectPath`, async (req, res) => {
  let project = await DocsTable.get({
    projectId: Number.parseInt(req.params.projectId)
  });
  if (project.kind !== "public") {
    return res.render("error");
  }
  let userEmail = project.userEmail;
  let projectDetails = await GitSource.getProject(
    userEmail,
    req.params.projectId
  );
  res.render("doc", { title: projectDetails.name });
});

router.get(`/public/:projectId/:projectPath/*`, async (req, res, next) => {
  let filePath = req.path.slice(
    `/public/${req.params.projectId}/${req.params.projectPath}`.length
  );
  let fileKey = `public/${path.join(
    req.params.projectId,
    req.params.projectPath,
    filePath
  )}`;

  s3.getObject({
    Bucket: "devportal-docs",
    Key: fileKey
  })
    .createReadStream()
    .on("error", next)
    .pipe(res);
});

router.get("/restricted/:projectId/:projectPath", secured, async (req, res) => {
  let project = await DocsTable.get({
    projectId: Number.parseInt(req.params.projectId)
  });
  if (project.kind !== "restricted") {
    return res.render("error");
  }
  let userEmail = project.userEmail;
  let projectDetails = await GitSource.getProject(
    userEmail,
    req.params.projectId
  );
  res.render("doc", { title: projectDetails.name });
});

router.get(
  "/restricted/:projectId/:projectPath/*",
  secured,
  async (req, res, next) => {
    let filePath = req.path.slice(
      `/restricted/${req.params.projectId}/${req.params.projectPath}`.length
    );

    let fileKey = `restricted/${path.join(
      req.params.projectId,
      req.params.projectPath,
      filePath
    )}`;
    s3.getObject({
      Bucket: "devportal-docs",
      Key: fileKey
    })
      .createReadStream()
      .on("error", next)
      .pipe(res);
  }
);

router.get("/restricted", secured, async (req, res) => {
  let restrictedProjects = await DocsTable.scan({
    kind: "restricted"
  });

  let projectDetails = await Promise.all(
    restrictedProjects.map(async restrictedProject => {
      let projectId = restrictedProject.projectId;
      let userEmail = restrictedProject.userEmail;
      return GitSource.getProject(userEmail, projectId);
    })
  );

  projectDetails.forEach(project => {
    project.docPortal = {
      path: path.join(`/restricted`, `${project.id}`, `${project.path}`)
    };
  });

  res.render("restricted", {
    title: "Restricted Docs",
    projects: projectDetails,
    isAuthenticated: req.isAuthenticated()
  });
});

module.exports = router;

const createError = require("http-errors");
const express = require("express");
const session = require("express-session");
const DynamoDBStore = require("connect-dynamodb")({ session });
const helmet = require("helmet");
const path = require("path");
const logger = require("morgan");

const config = require("./config");
const { passport, secured } = require("./lib/passport");

const indexRouter = require("./routes/index");
const authRouter = require("./routes/auth");
const accountRouter = require("./routes/account");
const gitSourceRouter = require("./routes/gitSource");
const webhookListenerRouter = require("./routes/webhookListener");

const app = express();

app.use(helmet());

// if (config.nodeEnv !== "production") {
//   app.use(
//     cors({
//       origin: /http:\/\/localhost(:\d+)?/,
//       credentials: true
//     })
//   );
// }

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");

app.use(logger("common"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, "public")));

app.use(
  session({
    store: new DynamoDBStore({
      table: config.dynamoSessionTable,
      AWSRegion: "ap-southeast-1",
      readCapacityUnits: 1,
      writeCapacityUnits: 1
    }),
    secret: "change this to a random string",
    name: config.sessionCookieName,
    cookie: {
      httpOnly: false
    },
    resave: false,
    saveUninitialized: true
  })
);

app.use(passport.initialize());
app.use(passport.session());

app.use("/", indexRouter);
app.use("/auth", authRouter);
app.use("/account", secured, accountRouter);
app.use("/git-source", secured, gitSourceRouter);
app.use(
  "/webhook-listener",
  (req, res, next) => {
    // TODO: Verify X-Gitlab-Token header
    next();
  },
  webhookListenerRouter
);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || err.statusCode || 500);
  res.render("error");
});

module.exports = app;

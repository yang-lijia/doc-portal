const _ = require("lodash");
const { dynamoClient } = require("../lib/aws");
const config = require("../config");

const DocsTable = {
  async scan(options) {
    // { kind: { S: 'public' } }

    let FilterExpression = ""; //`kind = :kind `
    let ExpressionAttributeValues = {};
    // {
    //   ":kind": {
    //     "S": "public"
    //   }
    // }

    Object.keys(options).forEach((key, index) => {
      FilterExpression +=
        index > 0 ? ` and ${key} = :${key}` : `${key} = :${key}`;
      ExpressionAttributeValues[`:${key}`] = options[key];
    });

    let params = {
      TableName: config.dynamoDocsTable,
      FilterExpression,
      ExpressionAttributeValues
    };

    return new Promise((resolve, reject) => {
      dynamoClient.scan(params, (err, data) => {
        if (err) {
          return reject(err);
        }
        if (_.isEmpty(data)) {
          return resolve(null);
        }
        resolve(data.Items);
      });
    });
  },
  async query(options) {
    let ExpressionAttributeValues = {};
    let KeyConditionExpression = "";

    Object.keys(options).forEach((key, index) => {
      ExpressionAttributeValues[`:${key}`] = options[key];
      KeyConditionExpression +=
        index > 0 ? ` and ${key} = :${key}` : `${key} = :${key}`;
    });
    let params = {
      TableName: config.dynamoDocsTable,
      ExpressionAttributeValues,
      KeyConditionExpression
    };

    return new Promise((resolve, reject) => {
      dynamoClient.query(params, (err, data) => {
        if (err) {
          return reject(err);
        }
        if (_.isEmpty(data)) {
          return resolve(null);
        }
        resolve(data.Items);
      });
    });
  },
  async get(options) {
    let params = {
      TableName: config.dynamoDocsTable,
      Key: {
        ...options
      }
    };
    return new Promise((resolve, reject) => {
      dynamoClient.get(params, (err, data) => {
        if (err) {
          return reject(err);
        }
        if (_.isEmpty(data)) {
          return resolve(null);
        }
        resolve(data.Item);
      });
    });
  },
  async put(item) {
    let params = {
      TableName: config.dynamoDocsTable,
      Item: {
        ...item
      }
    };
    return new Promise((resolve, reject) => {
      dynamoClient.put(params, (err, data) => {
        if (err) {
          return reject(err);
        }
        // data = {} if success, so return inserted item here.
        if (!_.isEmpty(data)) {
          return resolve(data);
        }
        resolve({ ...item });
      });
    });
  },
  async delete(options) {
    let params = {
      TableName: config.dynamoDocsTable,
      Key: {
        ...options
      }
    };
    return new Promise((resolve, reject) => {
      dynamoClient.delete(params, (err, data) => {
        if (err) {
          return reject(err);
        }
        if (!_.isEmpty(data)) {
          return resolve(data);
        }
        resolve({ ...options });
      });
    });
  }
};

module.exports = DocsTable;

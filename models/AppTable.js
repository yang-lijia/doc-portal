const _ = require("lodash");
const { dynamoClient } = require("../lib/aws");
const config = require("../config");

const AppTable = {
  async get(options) {
    return new Promise((resolve, reject) => {
      let params = {
        TableName: config.dynamoAppTable,
        Key: {
          ...options
        }
      };
      dynamoClient.get(params, (err, data) => {
        if (err) {
          return reject(err);
        }
        if (_.isEmpty(data)) {
          return resolve(null);
        }
        resolve(data.Item);
      });
    });
  },
  async put(item) {
    return new Promise((resolve, reject) => {
      let params = {
        TableName: config.dynamoAppTable,
        Item: {
          ...item
        }
      };
      dynamoClient.put(params, (err, data) => {
        if (err) {
          return reject(err);
        }
        // data = {} if success, so return inserted item here.
        if (!_.isEmpty(data)) {
          return resolve(data);
        }
        resolve({ ...item });
      });
    });
  }
};

module.exports = AppTable;
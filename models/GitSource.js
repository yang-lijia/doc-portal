const _ = require("lodash");
const axios = require("axios").default;
const config = require("../config");
const AppTable = require("./AppTable");

const ACCESS_LEVELS = {
  DEVELOPER: 30,
  MAINTAINER: 40,
  OWNER: 50
};

const gitSourceBaseURL = "https://gitlab.com/api/v4";

function gitSourceApiClient(userEmail) {
  const axiosClient = axios.create({
    baseURL: gitSourceBaseURL
  });
  return {
    async get(path, options = {}) {
      let accessToken = await getGitSourceAccessToken(userEmail);
      return axiosClient
        .request({
          method: "GET",
          url: path,
          headers: {
            Authorization: `Bearer ${accessToken}`
          },
          ...options
        })
        .then(response => response.data);
    },
    async post(path, options = {}) {
      let accessToken = await getGitSourceAccessToken(userEmail);
      return axiosClient
        .request({
          method: "POST",
          url: path,
          headers: {
            Authorization: `Bearer ${accessToken}`
          },
          ...options
        })
        .then(response => response.data);
    },
    async delete(path) {
      let accessToken = await getGitSourceAccessToken(userEmail);
      return axiosClient
        .request({
          method: "DELETE",
          url: path,
          headers: {
            Authorization: `Bearer ${accessToken}`
          }
        })
        .then(response => response.data);
    }
  };
}

const getGitSourceAccessToken = async userEmail => {
  let userAttributes = await AppTable.get({
    email: userEmail
  });
  if (!userAttributes.tokens || _.isEmpty(userAttributes.tokens)) {
    throw new Error("User has not authorized their Git source account yet.");
  }
  return userAttributes.tokens.access_token;
};

const GitSource = {
  async getProjects(userEmail) {
    let projects = await gitSourceApiClient(userEmail).get(`/projects`, {
      params: {
        simple: true,
        min_access_level: ACCESS_LEVELS.DEVELOPER
      }
    });

    return projects;
  },
  async getProject(userEmail, projectId) {
    let project = await gitSourceApiClient(userEmail).get(
      `/projects/${projectId}`
    );
    return project;
  },
  async getProjectHooks(userEmail, projectId) {
    let projectHooks = await gitSourceApiClient(userEmail).get(
      `/projects/${projectId}/hooks`
    );
    return projectHooks;
  },
  async addProjectHook(userEmail, projectId) {
    let addResult = await gitSourceApiClient(userEmail).post(
      `/projects/${projectId}/hooks`,
      {
        data: {
          url: config.gitSourceWebhookUrl,
          push_events: true,
          push_events_branch_filter: "master",
          token: config.gitSourceWebhookSecretToken
        }
      }
    );
    return addResult;
  },
  async deleteProjectHook(userEmail, projectId, hookId) {
    let deleteResult = await gitSourceApiClient(userEmail).delete(
      `/projects/${projectId}/hooks/${hookId}`
    );
    // If the project hook is available before it is returned in the
    // JSON response else an empty response is returned.
    return deleteResult;
  },
  async getProjectFiles(userEmail, projectId) {
    return gitSourceApiClient(userEmail).get(
      `/projects/${projectId}/repository/tree`,
      {
        params: {
          ref: "refs/heads/master",
          recursive: true,
          per_page: 50 // TODO: pagination?
        }
      }
    );
  },
  async getProjectFile(userEmail, projectId, filePath) {
    return gitSourceApiClient(userEmail).get(
      `/projects/${projectId}/repository/files/${encodeURIComponent(filePath)}`,
      {
        params: {
          ref: "refs/heads/master"
        }
      }
    );
  }
};

module.exports = GitSource;

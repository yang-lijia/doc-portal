## Local setup

To run the app locally, set up Dynamo tables for session and app tables in AWS:

```bash
serverless deploy --stage $STAGE --aws-profile $MYAWSPROFILE
# --stage determines table names and environment variables passed to app
# --aws-profile if using non-default aws credentials set up using aws-cli
```

Note the names of the tables as defined in serverless.yml (dependent on service name & stage name).

The app will reference these tables through environment variables. For example:

```bash
# .env, or use environment variables

NODE_ENV=local
APP_TABLE="doc-portal-$NODE_ENV"
SESSION_TABLE="doc-portal-sessions-$NODE_ENV"
```

### Remove deployment

```bash
serverless remove --stage $STAGE --aws-profile $MYAWSPROFILE
```

## Application structure
Run two shells to do real-time builds of both static (Vue) and backend (express) apps.

```bash
npm run dev:server
npm run dev:static
```
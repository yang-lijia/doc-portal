const passport = require("passport");
const path = require("path");
const OidcStrategy = require("@jywei/passport-openidconnect").Strategy;

const AppTable = require("../models/AppTable");
const config = require("../config");

passport.use(
  "oidc",
  // If not passing in tokenURL, lib will attempt to use oidc metadata document
  // https://github.com/jaredhanson/passport-openidconnect/blob/52d6874d1031dd9a1ed642871dffaba44050608b/lib/strategy.js#L43
  new OidcStrategy(
    {
      issuer: config.oidcIssuer, // Depends on specific provider
      authorizationURL: `https://${path.join(
        config.oidcDomain,
        config.oidcAuthorizationPath
      )}`,
      tokenURL: `https://${path.join(config.oidcDomain, config.oidcTokenPath)}`,
      userInfoURL:
        config.oidcUserinfoUrl ||
        `https://${path.join(config.oidcDomain, config.oidcUserinfoPath)}`,
      clientID: config.oidcClientId,
      clientSecret: config.oidcClientSecret,
      callbackURL: "http://localhost:4321/auth/callback", // Configure this exactly on the IDP, including http://
      scope: "openid profile email"
    },
    async (
      issuer,
      sub,
      profile,
      jwtClaims,
      accessToken,
      refreshToken,
      idToken,
      params,
      done
    ) => {
      // First time we get token/profile information.
      // User info or error passed to passport.authenticate.
      // If email not in app table, populate it
      let userEmail = profile._json.email;
      try {
        let user = await AppTable.get({
          email: userEmail
        });
        if (!user) {
          user = await AppTable.put({
            email: userEmail
          });
        }
        return done(null, {
          profile,
          idToken,
          accessToken,
          refreshToken,
          appUser: user
        });
      } catch (err) {
        console.error(err);
      }
    }
  )
);

// For serializing user into passport session
passport.serializeUser(function(user, done) {
  done(null, user);
});

passport.deserializeUser(function(user, done) {
  done(null, user);
});

function secured(req, res, next) {
  if (req.isAuthenticated()) {
    // If authenticated
    next();
  } else {
    // Else
    // return to home?
    res.redirect("/");
  }
}

module.exports = {
  passport,
  secured
};

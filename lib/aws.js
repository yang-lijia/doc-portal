const AWS = require("aws-sdk");

AWS.config.update({ region: "ap-southeast-1" });

const s3 = new AWS.S3({ apiVersion: "2006-03-01" });

const dynamoDB = new AWS.DynamoDB({ apiVersion: "2012-08-10" });

// 0-length strings, binary buffers, sets -> NULL
const dynamoClient = new AWS.DynamoDB.DocumentClient({
  convertEmptyValues: true
});

module.exports = {
  AWS,
  s3,
  dynamoDB,
  dynamoClient
};

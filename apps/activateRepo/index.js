import Vue from "vue";
import App from "./App.vue";
let mountElement = document.querySelector("#activate-repos")
new Vue({
  render: h => h(App, {
    props: {
      ...mountElement.dataset
    }
  })
}).$mount("#activate-repos");

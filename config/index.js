const nodeEnv = process.env.NODE_ENV || "development";
module.exports = {
  appPort: process.env.APP_PORT || 4321,
  nodeEnv,
  dynamoSessionTable: loadRequiredVariable("SESSION_TABLE"),
  dynamoAppTable: loadRequiredVariable("APP_TABLE"),
  dynamoDocsTable: loadRequiredVariable("DOCS_TABLE"),
  sessionCookieName: "doc.p.sid",
  oidcIssuer: loadRequiredVariable("OIDC_ISSUER"),
  oidcDomain: loadRequiredVariable("OIDC_DOMAIN"),
  oidcAuthorizationPath: loadRequiredVariable("OIDC_AUTHORIZATION_PATH"),
  oidcTokenPath: loadRequiredVariable("OIDC_TOKEN_PATH"),
  oidcUserinfoUrl: loadRequiredVariable("OIDC_USERINFO_URL"),
  oidcLogoutPath: loadRequiredVariable("OIDC_LOGOUT_PATH"),
  oidcClientId: loadRequiredVariable("OIDC_CLIENT_ID"),
  oidcClientSecret: loadRequiredVariable("OIDC_CLIENT_SECRET"),
  gitSourceAppId: loadRequiredVariable("GIT_SOURCE_APP_ID"),
  gitSourceAppSecret: loadRequiredVariable("GIT_SOURCE_APP_SECRET"),
  gitSourceWebhookUrl: loadRequiredVariable("GIT_SOURCE_WEBHOOK_URL"), // HTTPS POST endpoint for push events
  gitSourceWebhookSecretToken: loadRequiredVariable("GIT_SOURCE_WEBHOOK_SECRET_TOKEN") // Auth for webhook push
};

function loadRequiredVariable(name) {
  if (process.env[name] == null || process.env[name] === "") {
    throw new Error("Environment variable " + name + " is required!");
  }
  return process.env[name];
}
